#include "driver.h"

volatile boolean driver::yd = true;

volatile boolean driver::xl = true;

double driver::f = 31250.0d;

double volatile driver::x = 0.0d;
double volatile driver::y = 0.0d;

void driver::begin() {
  pinMode(PIN_DIRY, OUTPUT); pinMode(PIN_PULY, OUTPUT); pinMode(PIN_ENAY, OUTPUT);
  pinMode(PIN_RESETY, INPUT_PULLUP);
  digitalWrite(PIN_ENAY, HIGH);
  analogWrite(PIN_PULY, 0);

  pinMode(PIN_DIRX, OUTPUT); pinMode(PIN_PULX, OUTPUT); pinMode(PIN_ENAX, OUTPUT);
  pinMode(PIN_RESETX, INPUT_PULLUP);
  digitalWrite(PIN_ENAX, HIGH);
  analogWrite(PIN_PULX, 0);
  //setPwmFrequency(PIN_PULX, PWM_RATIO_X);
  //calibrateY();
  //calibrateX();
}

void driver::setFrequency(double freq)
{
  driver::f = freq;
}

void driver::moveX(double pos){
  //Check direction
  setPwmFrequency(PIN_PULX, PWM_RATIO_X);
  boolean dir = 0;
  double distance = pos - x;
  if(distance < 0){
    dir = 1; distance*=-1;
  }
  double tf = f / PWM_RATIO_X;
  double t = distance / tf / DISTANCE_PER_CYCLE_X;
  double dt = round(t * 1000.0);
  Serial.print(dt);
  digitalWrite(PIN_ENAX, LOW);
  delay(1);
  digitalWrite(PIN_DIRX, dir);
  delay(1);
  analogWrite(PIN_PULX, 127);
  delay(dt);
  analogWrite(PIN_PULX, 0);
  digitalWrite(PIN_ENAX, HIGH);
  x = pos;
}

void driver::moveY(double pos) {
  setPwmFrequency(PIN_PULY, PWM_RATIO_Y);
  //Check direction
  boolean dir = 0;
  double distance = pos - y;
  if(distance < 0){
    dir = 1; distance*=-1;
  }
  double tf = f / PWM_RATIO_Y;
  double t = distance / tf / DISTANCE_PER_CYCLE_Y;
  Serial.print("t: ");
  double dt = round(t * 1000.0);
  Serial.print(dt);
  digitalWrite(PIN_ENAY, LOW);
  delay(1);
  digitalWrite(PIN_DIRY, dir);
  delay(1);
  analogWrite(PIN_PULY, 127);
  delay(dt);
  analogWrite(PIN_PULY, 0);
  digitalWrite(PIN_ENAY, HIGH);
  y = pos;
  delay(50);
}

void driver::calibrateX(){
    setPwmFrequency(PIN_PULX, PWM_RATIO_RETURN_X);
    //attachInterrupt(digitalPinToInterrupt(PIN_RESETX), XISR, LOW);

    if(!digitalRead(PIN_RESETX)){
        return;
    }
    
    driver::xl = true;
    digitalWrite(PIN_ENAX, LOW);
    delay(1);
    digitalWrite(PIN_DIRX, 1);
    delay(1);
    analogWrite(PIN_PULX, 127);

    bool oldFlag = 1;
    long startTime = 0;
    while(true){
       bool newFlag = digitalRead(PIN_RESETX);
       if (!newFlag && oldFlag){
          startTime = millis();
          oldFlag = newFlag;
          continue;
       }
       if (newFlag && !oldFlag){
          startTime = 0;
          oldFlag = 1;
          continue;
       }
       long currentTime = millis();
       if (newFlag == oldFlag && !newFlag && (currentTime - startTime > 100)){
          break; 
       }
    }
    //delay(1000);
    digitalWrite(PIN_ENAX, HIGH);
    analogWrite(PIN_PULX, 0);
    driver::x = 0.0;
    //detachInterrupt(digitalPinToInterrupt(PIN_RESETX));
    delay(1000);
}

void driver::calibrateY(){
    setPwmFrequency(PIN_PULY, PWM_RATIO_RETURN_Y);
    //attachInterrupt(digitalPinToInterrupt(PIN_RESETY), YISR, LOW);
    driver::yd = true;
    digitalWrite(PIN_ENAY, LOW);
    delay(1);
    digitalWrite(PIN_DIRY, 1);
    delay(1);
    analogWrite(PIN_PULY, 127);
    bool oldFlag = 1;
    long startTime = 0;
    while(true){
       bool newFlag = digitalRead(PIN_RESETY);
       if (!newFlag && oldFlag){
          startTime = millis();
          oldFlag = newFlag;
          continue;
       }
       if (newFlag && !oldFlag){
          startTime = 0;
          oldFlag = 1;
          continue;
       }
       long currentTime = millis();
       if (newFlag == oldFlag && !newFlag && (currentTime - startTime > 1)){
          break; 
       }
    }
    //delay(1000);
    digitalWrite(PIN_ENAY, HIGH);
    analogWrite(PIN_PULY, 0);
    driver::y = 0.0;
    //detachInterrupt(digitalPinToInterrupt(PIN_RESETY));
    delay(1000);
}

void driver::YISR() {
  Serial.print("YISR");
  if (yd == true) { //do not turn off if already off
    yd = false; //down is invalid
  }
}

void driver::XISR() {
  //Serial.print("XISR");
  if (xl == true) { //do not turn off if already off
    xl = false; //right is invalid
  }
}

void driver::setPwmFrequency(int pin, int divisor) {
  byte mode;
  if (pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch (divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if (pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if (pin == 3 || pin == 11) {
    switch (divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
