#ifndef __DRIVER_H__
#define __DRIVER_H__

#include <arduino.h>

//Pins
#define PIN_RESETY 3
#define PIN_RESETX 2
#define PIN_PULY 9
#define PIN_PULX 10
#define PIN_DIRY 7
#define PIN_DIRX 8
#define PIN_ENAY 12
#define PIN_ENAX 13

//Configurations
#define PWM_RATIO_Y 256.0
#define PWM_RATIO_X 64.0
#define PWM_RATIO_RETURN_Y 64.0
#define PWM_RATIO_RETURN_X 64.0

//Constants
#define DISTANCE_PER_CYCLE_Y 0.00010795
#define DISTANCE_PER_CYCLE_X 0.00012000

class driver {

  public:
    static void moveY(double y);
    static void moveX(double x);

    static void setFrequency(double f);

    static void begin();

    static void calibrateY();
    static void calibrateX();

  private:
    static void setPwmFrequency(int pin, int divider);
    static void YISR();
    static void XISR();
    static volatile double x;
    static volatile double y;

    static double f;
    static volatile boolean yd;
    static volatile boolean xl;
};

#endif
