#ifndef __COMMANDER_H__
#define __COMMANDER_H__

#include <arduino.h>
#include "driver.h"

/**
 * Commander accepts command line-by-line in the format of: command [param1] [param2]...
 * It will then run the corresponding driver to send command to the actuator.
 */
class commander {
public:
	static void begin();
	static void decode(String s);
};

#endif
