#include "commander.h"

void commander::begin(){
	Serial.begin(9600);
	driver::begin();
}

void commander::decode(String str){
	if(str.indexOf("mpy")!=-1){ //move position
		String pos = str.substring(str.indexOf(" ")+1,str.length());
		driver::moveY(pos.toInt());
	}
}
