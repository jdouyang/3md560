int ENA = 13, DIR = 8, PUL =10, RESET_Y = 2;

double k = 0.00014708;

double f = 31250.0d;

#ifndef RIGHT
#define RIGHT 0
#endif

#ifndef LEFT
#define LEFT 1
#endif

boolean yd = true;

void setup() {

  Serial.begin(9600);
  
  // put your setup code here, to run once:
  pinMode(DIR,OUTPUT); pinMode(PUL,OUTPUT); pinMode(ENA,OUTPUT);

  pinMode(RESET_Y, INPUT_PULLUP);


  digitalWrite(ENA, HIGH);
  
  //attachInterrupt(digitalPinToInterrupt(RESET_Y), YISR, LOW); 
//  pinMode(RESET_Y, INPUT_PULLUP);
  double r = 64.0;
  f = f/r;
  setPwmFrequency(PUL,r);
  
// moveYd(DOWN,0.0992);
// moveYd(LEFT,0.05);
  moveY(RIGHT,2000);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1000);
  if(digitalRead(RESET_Y)==0){
    digitalWrite(ENA, HIGH);
    analogWrite(PUL,0);
    yd = false; //down is invalid
  }
}

void moveYd(boolean down, double distance){
  if(down && yd == false){
    digitalWrite(ENA, HIGH);
    analogWrite(PUL,0);
    return;
  }

  double t = distance/f/k;
  double dt = round(t*1000.0);
  digitalWrite(ENA, LOW);
  delay(1);
  digitalWrite(DIR,down);
  delay(1);
  analogWrite(PUL,127);
  delay(dt);
  analogWrite(PUL,0);
  digitalWrite(ENA, HIGH);
  delay(500);
}

void moveY(boolean down, int ms){
//  unsigned long new_time=millis()+ms;
  if(down && yd == false){
    digitalWrite(ENA, HIGH);
    analogWrite(PUL,0);
    return;
  }

  Serial.print("MOVING ");
  Serial.print(down?"DOWN":"UP");

  digitalWrite(ENA, LOW);
  delay(1);
  digitalWrite(DIR,down);
  delay(1);
  analogWrite(PUL,127);
//  Serial.println("Prepare to run");
//  while(millis()<new_time){
//    
//  }
//  Serial.println("end to run");
  delay(ms);
  analogWrite(PUL,0);
  digitalWrite(ENA, HIGH);
  delay(500);
}


//shuts off Y-axis
void YISR(){
  Serial.println("Interrupted");
  if(yd == true){ //do not turn off if already off
    digitalWrite(ENA, HIGH);
    analogWrite(PUL,0);
    yd = false; //down is invalid
  }
}

/**
 * Divides a given PWM pin frequency by a divisor.
 *
 * The resulting frequency is equal to the base frequency divided by
 * the given divisor:
 *   - Base frequencies:
 *      o The base frequency for pins 3, 9, 10, and 11 is 31250 Hz.
 *      o The base frequency for pins 5 and 6 is 62500 Hz.
 *   - Divisors:
 *      o The divisors available on pins 5, 6, 9 and 10 are: 1, 8, 64,
 *        256, and 1024.
 *      o The divisors available on pins 3 and 11 are: 1, 8, 32, 64,
 *        128, 256, and 1024.
 *
 * PWM frequencies are tied together in pairs of pins. If one in a
 * pair is changed, the other is also changed to match:
 *   - Pins 5 and 6 are paired on timer0
 *   - Pins 9 and 10 are paired on timer1
 *   - Pins 3 and 11 are paired on timer2
 *
 * Note that this function will have side effects on anything else
 * that uses timers:
 *   - Changes on pins 3, 5, 6, or 11 may cause the delay() and
 *     millis() functions to stop working. Other timing-related
 *     functions may also be affected.
 *   - Changes on pins 9 or 10 will cause the Servo library to function
 *     incorrectly.
 *
 * Thanks to macegr of the Arduino forums for his documentation of the
 * PWM frequency divisors. His post can be viewed at:
 *   http://forum.arduino.cc/index.php?topic=16612#msg121031
 */
void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x07; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
